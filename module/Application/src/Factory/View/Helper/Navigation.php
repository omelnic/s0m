<?php

namespace Application\Factory\View\Helper;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;

/**
 * Class Navigation
 *
 * @author  nonick <web@nonick.name>
 * @package Application\Factory\View\Helper
 */
class Navigation implements DelegatorFactoryInterface
{
    /**
     * A factory that creates delegates of a given service
     *
     * @param  ContainerInterface $container
     * @param  string             $name
     * @param  callable           $callback
     * @param  null|array         $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var \Zend\View\Helper\Navigation $navigation */
        $navigation = $callback();

        /**
         * @TODO раскомментировать после подключения и настройки BjyAuthorizeServiceAuthorize
         */
//        $authorize = $container->get('BjyAuthorizeServiceAuthorize');
//        $acl = $authorize->getAcl();
//        $role = $authorize->getIdentity();
//        $navigation->setAcl($acl);
//        $navigation->setRole($role);

        return $navigation;
    }
}
