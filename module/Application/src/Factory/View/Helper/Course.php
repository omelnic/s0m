<?php

namespace Application\Factory\View\Helper;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Course\Chapter as ChapterService;
use Application\Service\Course\Item\Item as ItemService;

/**
 * Class Course
 *
 * @package Application\Factory\View\Helper
 */
class Course implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $course = new \Application\View\Helper\Course();
        $course->setChapterService($container->get(ChapterService::class));
        $course->setItemService($container->get(ItemService::class));
        return $course;
    }
}
