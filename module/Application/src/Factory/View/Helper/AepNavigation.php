<?php

namespace Application\Factory\View\Helper;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class AepNavigation
 *
 * @author  nonick <web@nonick.name>
 * @package Application\Factory\View\Helper
 */
class AepNavigation implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $navigation = new \Application\View\Helper\AepNavigation();
        /** @var \Application\Navigation\ContainerProvider $provider */
        $provider = $container->get(\Application\Navigation\ContainerProvider::class);
        $navigation->setContainerProvider($provider);

        return $navigation;
    }
}
