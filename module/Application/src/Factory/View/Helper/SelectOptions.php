<?php

namespace Application\Factory\View\Helper;

use Application\Service\Course\Course;
use Application\Service\User\User;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Course\Category;

/**
 * Class Course
 *
 * @package Application\Factory\View\Helper
 */
class SelectOptions implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $category = new \Application\View\Helper\SelectOptions();
        $category->setCategoryService($container->get(Category::class));
        $category->setUserService($container->get(User::class));
        $category->setCourseService($container->get(Course::class));
        return $category;
    }
}