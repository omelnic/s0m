<?php
/**
 * @author Topuria Evgheni
 */

namespace Application\Factory\Exception;

class InvalidArgument extends \RuntimeException implements ExceptionInterface
{
}
