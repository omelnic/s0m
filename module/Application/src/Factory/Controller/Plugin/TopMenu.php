<?php
/**
 * @author Oleg Melnic
 */

namespace Application\Factory\Controller\Plugin;

use Application\Factory\Navigation\Partial;
use Application\Navigation\ContainerProvider;
use Application\Controller\Plugins\InjectMenu;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class Menu
 * @package Application\Factory\Controller\Plugin
 */
class TopMenu implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @return InjectMenu
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Application\Navigation\PluginManager $pluginManager */
        $pluginManager = $container->get('injectedNavigationPluginManager');
        $service = new InjectMenu();
        $service->setMenuContainer($this->getMenuContainer($container));
        $service->setPluginManager($pluginManager);

        return $service;
    }

    /**
     * @param ContainerInterface $container
     * @return null|\Zend\Navigation\Page\AbstractPage
     */
    private function getMenuContainer(ContainerInterface $container)
    {
        $mainNavigationName = Partial::MAIN_NAVIGATION_PREFIX.'aep';
        /** @var \Zend\Navigation\AbstractContainer $containerNavigation */
        $containerNavigation = $container->get($mainNavigationName);

        return $containerNavigation->findBy('id', ContainerProvider::TOP_MENU_ALIAS);
    }
}
