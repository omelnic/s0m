<?php

namespace Application\Factory\Navigation;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

/**
 * Class Partial
 *
 * @author  nonick <web@nonick.name>
 * @package Application\Factory\Navigation
 */
class Partial implements AbstractFactoryInterface
{
    /**
     * Service manager factory prefix
     * Сервис создается через абстрактную фабрику с приставкой языка, в конфиге главный узел конфига навигации
     * @var string
     */
    const MAIN_NAVIGATION_PREFIX = 'Zend\Navigation\\Navigation';

    const SERVICE_PREFIX = 'Aep\Navigation\\';

    const FULL_NAVIGATION = 'full';

    /**
     * Navigation configuration
     *
     * @var array
     */
    protected $config;

    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     *
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        if (0 !== strpos($requestedName, self::SERVICE_PREFIX)) {
            return false;
        }

        return true;
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $alias = $this->getConfigName($requestedName);
        /**
         * @todo при замене приставки 'aep' на алиас языка, изменить главный узел в конфиге navigation.global.php
         */
        $mainNavigationName = self::MAIN_NAVIGATION_PREFIX.'aep';

        /** @var \Zend\Navigation\AbstractContainer $containerNavigation */
        $containerNavigation = $container->get($mainNavigationName);
        if ($alias == self::FULL_NAVIGATION) {
            return $containerNavigation;
        }
        $menu = $containerNavigation->findBy('id', $alias);

        return $menu;
    }

    /**
     * Extract config name from service name
     *
     * @param string $name
     * @return string
     */
    protected function getConfigName($name)
    {
        return substr($name, strlen(self::SERVICE_PREFIX));
    }
}
