<?php
/**
 * Класс отправки писем через sendmail()
 */
namespace Application\Factory\ServiceMail;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Mail\Transport\Sendmail;

class SendMailTransport implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new Sendmail();
    }
}
