<?php
namespace Application\InputFilterBuilder\Course;

use S0mWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class Course
 *
 * @copyright (c) 2017, S0m
 */
class CourseCreate extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('name', true, 'base.text');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
