<?php
namespace Application\InputFilterBuilder\Course\Item\Strategy;

use Application\Entity\Course\Item\Quiz\QuizVO;
use Application\InputFilterBuilder\Course\Item\ItemAbstract;
use Application\Validator\Instance;

/**
 * Class Article
 *
 * @author Shahnovsky Alex
 */
class Quiz extends ItemAbstract
{
    /**
     * Метод использующийся для дополнительной настройки конфига InputFilter
     * @return void
     */
    protected function customInit()
    {
        $this->getInputFilterBuilder()->add(
            'quiz',
            true,
            null,
            [
                [
                    'name' => 'ToNull',
                ],
                [
                    'name' => 'CreateFromArray',
                    'options' => [
                        'object_name' => QuizVO::class,
                    ],
                ],
            ],
            [
                [
                    'name' => 'Instance',
                    'options' => [
                        'allowNull' => true,
                        'className' => QuizVO::class,
                        'messages' => [
                            Instance::NOT_INSTANCE_OF => 'Please fill all the qiuz fields',
                        ],
                    ],
                ],
            ]
        );
    }
}
