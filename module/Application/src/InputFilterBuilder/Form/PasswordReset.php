<?php

namespace Application\InputFilterBuilder\Form;

use S0mWeb\WTL\InputFilterBuilder\BuilderAbstract;

/**
 * Class PasswordReset
 *
 * @copyright (c) 2017, S0m
 */
class PasswordReset extends BuilderAbstract
{

    /**
     * @return array
     */
    protected function build()
    {
        $this->getInputFilterBuilder()->add('email', true, 'base.email');
    }

    /**
     * Метод использующийся для настройки конфига InvariantInputFilter
     * @return mixed
     */
    protected function buildInvariant()
    {
    }
}
