<?php
namespace Application\View\Helper;

use S0mWeb\WTL\Entity\File\Image;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ImageThumb
 * @method \Application\View\Renderer getView()
 */
class ImageThumb extends AbstractHelper
{
    /**
     * @var \S0mWeb\WTL\Service\File\Image
     */
    protected $imageService;

    /**
     * @param Image $image
     * @param  int  $width
     * @param  int  $height
     * @param  string  $defaultImageSrc
     * @return string
     */
    public function __invoke($image, $width, $height, $defaultImageSrc = null)
    {
        if (null === $image) {
            return $defaultImageSrc;
        }
        $thumbNail = $this->getImageService()->getThumbNail($image, $width, $height);

        if (null === $thumbNail) {
            return $defaultImageSrc;
        }

        return $this->getView()->image($thumbNail)->getImgSrc();
    }

    /**
     * @return \S0mWeb\WTL\Service\File\Image
     */
    public function getImageService()
    {
        return $this->imageService;
    }

    /**
     * @param \S0mWeb\WTL\Service\File\Image $imageService
     */
    public function setImageService($imageService)
    {
        $this->imageService = $imageService;
    }
}
