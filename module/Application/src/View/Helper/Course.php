<?php
namespace Application\View\Helper;

use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;
use Application\Entity\Course\Course as CourseEntity;
use Application\Entity\Course\Chapter as ChapterEntity;
use Application\Service\Course\Chapter as ChapterService;
use Application\Service\Course\Item\Item as ItemService;

/**
 * Class Course
 * @method Renderer getView()
 */
class Course extends AbstractHelper
{
    /**
     * @var CourseEntity
     */
    protected $course;

    /**
     * @var ChapterService
     */
    protected $chapterService;

    /**
     * @var ItemService
     */
    protected $itemService;

    /**
     * @param CourseEntity $course
     * @return CourseEntity
     */
    public function __invoke($course)
    {
        $this->setCourse($course);
        return $this;
    }

    /**
     * @return CourseEntity
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param CourseEntity $course
     */
    public function setCourse(CourseEntity $course)
    {
        $this->course = $course;
    }

    /**
     * @return ChapterService
     */
    public function getChapterService()
    {
        return $this->chapterService;
    }

    /**
     * @param ChapterService $chapterService
     */
    public function setChapterService(ChapterService $chapterService)
    {
        $this->chapterService = $chapterService;
    }

    /**
     * @return ItemService
     */
    public function getItemService()
    {
        return $this->itemService;
    }

    /**
     * @param ItemService $itemService
     */
    public function setItemService(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * @return ChapterEntity[]|null
     */
    public function getAllChapters()
    {
        $courseId = $this->getCourse()->getIdentity();
        $chapters = $this->getChapterService()->getAllChaptersByCourse($courseId);
        return $chapters;
    }

    /**
     * @return integer
     */
    public function countQuizesByCourse()
    {
        $courseId = $this->getCourse()->getIdentity();
        return $this->getItemService()->countQuizesByCourse($courseId);
    }

    /**
     * @return integer
     */
    public function countIssuesByCourse()
    {
        $courseId = $this->getCourse()->getIdentity();
        return $this->getItemService()->countIssuesByCourse($courseId);
    }

    /**
     * Get order number for current chapter from list of chapters that belong to course
     *
     * @param ChapterEntity $chapter
     * @return integer|null
     */
    public function getChapterOrderedHeader($chapter)
    {
        $chapters = $this->getAllChapters();
        $chapterOrder = null;
        $i=1;
        foreach ($chapters as $one) {
            if ($one->getIdentity() == $chapter->getIdentity()) {
                $chapterOrder = $i;
                break;
            }
            $i++;
        }
        return "CHAPTER ".$chapterOrder;
    }
}
