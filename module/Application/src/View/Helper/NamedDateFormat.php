<?php
namespace Application\View\Helper;

use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;

/**
 * Class NamedDateFormat
 * @method Renderer getView()
 */
class NamedDateFormat extends AbstractHelper
{
    const FORMAT_MEDIUM_DATE = 'format_medium_date'; // April 27, 2017

    const FORMAT_MEDIUM_TIME = 'format_medium_time'; // 7:00 PM

    const FORMAT_MEDIUM_DATETIME = 'format_medium_datetime'; // April 27, 2017 | 7:00 PM

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @param \DateTime $date
     * @return NamedDateFormat
     */
    public function __invoke(\DateTime $date)
    {
        $this->setDate($date);
        return $this;
    }

    /**
     * @param \DateTime|null $date
     * @param  string $format
     * @param null|string $default
     *
     * @return string
     */
    public function getDateFormat($format, $default = null)
    {
        if (!$this->getDate() instanceof \DateTime) {
            return $default;
        }
        switch ($format) {
            case self::FORMAT_MEDIUM_DATE:
                return $this->getDate()->format('F j, Y');
            case self::FORMAT_MEDIUM_TIME:
                return $this->getDate()->format('g:i A');
            case self::FORMAT_MEDIUM_DATETIME:
                return $this->getDate()->format('F j, Y').' | '.$this->getDate()->format('g:i A');
            default:
                throw new \InvalidArgumentException(sprintf('Wrong format, %s given', $format));
        }
    }
}
