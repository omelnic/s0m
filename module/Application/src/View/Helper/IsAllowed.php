<?php
namespace Application\View\Helper;

use CirclicalUser\Provider\ResourceInterface;
use CirclicalUser\Service\AccessService;
use Zend\View\Helper\AbstractHelper;

/**
 * Class IsAllowed
 *
 * @author Shahnovsky Alex
 */
class IsAllowed extends AbstractHelper
{
    /**
     * @var AccessService
     */
    protected $accessService;

    /**
     * @return AccessService
     */
    public function getAccessService()
    {
        return $this->accessService;
    }

    /**
     * @param AccessService $accessService
     */
    public function setAccessService($accessService)
    {
        $this->accessService = $accessService;
    }

    /**
     * @param ResourceInterface|string $resource
     * @param string $action
     *
     * @return bool
     */
    public function __invoke($resource, $action)
    {
        return $this->getAccessService()->isAllowed($resource, $action);
    }
}
