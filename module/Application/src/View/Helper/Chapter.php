<?php
namespace Application\View\Helper;

use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;
use Application\Entity\Course\Chapter as ChapterEntity;
use Application\Entity\Course\Item\ItemAbstract as ItemEntity;
use Application\Service\Course\Item\Item as ItemService;
use Application\Entity\Course\Item\Quiz;


/**
 * Class Chapter
 * @method Renderer getView()
 */
class Chapter extends AbstractHelper
{
    /**
     * @var  ChapterEntity
     */
    protected $chapter;

    /**
     * @var ItemService
     */
    protected $itemService;

    /**
     * @param ChapterEntity $chapter
     * @return ChapterEntity
     */
    public function __invoke($chapter)
    {
        $this->setChapter($chapter);
        return $this;
    }

    /**
     * @return ChapterEntity
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * @param ChapterEntity $chapter
     */
    public function setChapter(ChapterEntity $chapter)
    {
        $this->chapter = $chapter;
    }

    /**
     * @return ItemService
     */
    public function getItemService()
    {
        return $this->itemService;
    }

    /**
     * @param ItemService $itemService
     */
    public function setItemService(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * @return ItemEntity[]|null
     */
    public function getAllItems()
    {
        $chapterId = $this->getChapter()->getIdentity();
        $items = $this->getItemService()->getAllItemsByChapter($chapterId);
        return $items;
    }

    /**
     * @return ItemEntity[]|null
     */
    public function getAllIssues()
    {
        $chapterId = $this->getChapter()->getIdentity();
        $issues = $this->getItemService()->getAllIssuesByChapter($chapterId);
        return $issues;
    }

    /**
     * @return ItemEntity[]|null
     */
    public function getAllQuizes()
    {
        $chapterId = $this->getChapter()->getIdentity();
        $quizes = $this->getItemService()->getAllQuizesByChapter($chapterId);
        return $quizes;
    }

    /**
     * Get header with order number for current item from list of items that belong to chapter
     *
     * @param ItemEntity $item
     * @return integer|null
     */
    public function getItemOrderedHeader($item)
    {
        $itemOrder = null;
        if ($item instanceof Quiz) {
            $items = $this->getAllQuizes();
            if (count($items) == 1) {
                return "Final Quiz";
            }
            $headerStr = "Quiz";
        } else {
            $items = $this->getAllIssues();
            $headerStr = "Issue";
        }

        $i=1;
        foreach ($items as $one) {
            if ($one->getIdentity() == $item->getIdentity()) {
                $itemOrder = $i;
                break;
            }
            $i++;
        }

        return $headerStr." ".$itemOrder;
    }
}
