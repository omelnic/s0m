<?php
namespace Application\View\Helper;

use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;
use Application\Entity\Event\Offline as OfflineEventEntity;
use Application\Service\User\User as UserService;
use Application\Entity\User\Student;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class OfflineEvent
 * @method Renderer getView()
 */
class OfflineEvent extends AbstractHelper
{
    /**
     * @var  OfflineEventEntity
     */
    protected $offlineEvent;

    /**
     * @var  UserService
     */
    protected $userService;

    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @param OfflineEventEntity $event
     * @return OfflineEvent
     */
    public function __invoke($event)
    {
        $this->setOfflineEvent($event);
        return $this;
    }

    /**
     * @return OfflineEventEntity
     */
    public function getOfflineEvent()
    {
        return $this->offlineEvent;
    }

    /**
     * @param OfflineEventEntity $provider
     */
    public function setOfflineEvent(OfflineEventEntity $offlineEvent)
    {
        $this->offlineEvent = $offlineEvent;
    }

    /**
     * @return UserService
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * @param UserService $userService
     */
    public function setUserService($userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * @param AuthenticationService $authenticationService
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @return Student[]
     */
    public function getUsers()
    {
        $currentUserId = $this->getAuthenticationService()->getIdentity()->getIdentity();
        $users = $this->getUserService()->getUsersByOfflineEvents($this->getOfflineEvent()->getIdentity(), $currentUserId);
        return $users;
    }
}
