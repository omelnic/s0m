<?php
/**
 * Created by Topuria Evgheni.
 * Date: 6/19/17
 */

namespace Application\View\Helper;

use Application\Entity\Course\Course;
use Application\Service\Course\Course as courseService;
use Application\Entity\Course\Difficulty;
use Application\Entity\ValueObject\Period;
use Application\Service\Course\Category;
use Application\Service\User\User;
use Zend\View\Helper\AbstractHelper;

class SelectOptions extends AbstractHelper
{
    /**
     * @var Category
     */
    private $categoryService;

    /**
     * @var User
     */
    private $userService;

    /**
     * @var courseService
     */
    private $courseService;


    public function getPeriodValues($value = null)
    {
        $arr = [
            Period::PERIOD_ONE_YEAR => 'One Year',
            Period::PERIOD_HALF_YEAR => 'Half a Year',
            Period::PERIOD_THREE_MONTH => 'Three Months',
            Period::PERIOD_ONE_MONTH => 'One Month',
        ];

        if ($value && $arr[$value]) {
            return $arr[$value];
        }

        return $arr;
    }

    public function getDifficultyValues($value = null)
    {
        $arr = [
            Difficulty::BEGINNER => 'Beginner',
            Difficulty::INTERMEDIATE => 'Intermediate',
            Difficulty::ADVANCED => 'Advanced',
        ];

        if ($value && $arr[$value]) {
            return $arr[$value];
        }

        return $arr;
    }

    public function getCategoriesValues($value = null)
    {
        $arr = [];
        $allCourseCategories = $this->getCategoryService()->getAllCourseCategories();

        if ($allCourseCategories) {
            foreach ($allCourseCategories as $category) {
                $arr[$category->getIdentity()] = $category->getName();
            }
        }

        if ($value && $arr[$value]) {
            return $arr[$value];
        }

        return $arr;
    }

    public function getProfessorsValues($value = null)
    {
        $arr = [];
        $allProfessors = $this->getUserService()->getAll(User::TYPE_PROFESSOR);

        if ($allProfessors) {
            foreach ($allProfessors as $professor) {
                $arr[$professor->getIdentity()] = $professor->getFullName();
            }
        }

        if ($value && $arr[$value]) {
            return $arr[$value];
        }

        return $arr;
    }

    public function getAllCourses($value = null)
    {
        $arr = [];
        $allCourses = $this->getCourseService()->findAll();

        if ($allCourses) {
            /** @var Course $course */
            foreach ($allCourses as $course) {
                $arr[$course->getIdentity()] = $course->getName();
            }
        }

        if ($value && $arr[$value]) {
            return $arr[$value];
        }

        return $arr;
    }

    public function getIssueTypeValues($value = null)
    {
        $arr = [
            'article' => 'Article',
            'video' => 'Video',
            'audio' => 'Audio',
            'quiz' => 'Quiz',
            'image' => 'Image',
            'document' => 'Document',
        ];

        if ($value && $arr[$value]) {
            return $arr[$value];
        }

        return $arr;
    }

    /**
     * @return Category
     */
    public function getCategoryService()
    {
        return $this->categoryService;
    }

    /**
     * @param Category $categoryService
     */
    public function setCategoryService($categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return mixed
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * @param mixed $professorService
     */
    public function setUserService($userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return courseService
     */
    public function getCourseService(): courseService
    {
        return $this->courseService;
    }

    /**
     * @param courseService $courseService
     */
    public function setCourseService(courseService $courseService)
    {
        $this->courseService = $courseService;
    }

    /**
     * Get timezone list
     *
     * @return array
     */
    public function getTimezonesList()
    {
        $zones = timezone_identifiers_list();

        foreach ($zones as $zone)
        {
            $zoneExploded = explode('/', $zone); // 0 => Continent, 1 => City

            // Only use "friendly" continent names
            if ($zoneExploded[0] == 'Africa' || $zoneExploded[0] == 'America' || $zoneExploded[0] == 'Antarctica' || $zoneExploded[0] == 'Arctic' || $zoneExploded[0] == 'Asia' || $zoneExploded[0] == 'Atlantic' || $zoneExploded[0] == 'Australia' || $zoneExploded[0] == 'Europe' || $zoneExploded[0] == 'Indian' || $zoneExploded[0] == 'Pacific')
            {
                if (isset($zoneExploded[1]) != '')
                {
                    if (!isset($locations[$zoneExploded[0]])) {
                        $locations[$zoneExploded[0]] = ['label' => $zoneExploded[0], 'options' => []];
                    }

                    $area = str_replace('_', ' ', $zoneExploded[1]);

                    if (!empty($zoneExploded[2]))
                    {
                        $area = $area . ' (' . str_replace('_', ' ', $zoneExploded[2]) . ')';
                    }

                    $locations[$zoneExploded[0]]['options'][$zone] = $area; // Creates array(DateTimeZone => 'Friendly name')
                }
            }
        }
        return $locations;
    }
}