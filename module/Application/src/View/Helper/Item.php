<?php
namespace Application\View\Helper;

use Application\View\Renderer;
use Zend\View\Helper\AbstractHelper;
use Application\Entity\Course\Item\ItemAbstract as ItemEntity;
use Application\Service\Course\Item\Item as ItemService;
use Application\Entity\Course\Item\Article;
use Application\Entity\Course\Item\Quiz;
use Application\Entity\Course\Item\Video;

/**
 * Class Item
 * @method Renderer getView()
 */
class Item extends AbstractHelper
{
    /**
     * @var ItemService
     */
    protected $itemService;

    /**
     * @param ItemEntity $chapter
     * @return ItemEntity
     */
    public function __invoke($item)
    {
        $this->setItem($item);
        return $this;
    }

    /**
     * @return ItemEntity
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param ItemEntity $item
     */
    public function setItem(ItemEntity $item)
    {
        $this->item = $item;
    }

    /**
     * @return ItemService
     */
    public function getItemService()
    {
        return $this->itemService;
    }

    /**
     * @param ItemService $itemService
     */
    public function setItemService(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * @param $iconClassChapter
     * @return string
     */
    public function getItemTypeInfo($iconClassChapter=null)
    {
        if (!is_null($iconClassChapter)) {
            switch ($iconClassChapter) {
                case "final" :
                    $info = ["iconClassChapter" => "final", "iconClassItem" => "fa-question-circle-o", "partialView" => "personal-office/item/quiz/view"];
                    break;
                case "video" :
                    $info = ["iconClassChapter" => "video", "iconClassItem" => "fa-video-circle-o", "partialView" => "personal-office/item/video/view"];
                    break;
                default:
                    $info = ["iconClassChapter" => "text", "iconClassItem" => "fa-file-text-o", "partialView" => "personal-office/item/article/view"];
                    break;
            }
        } else {
            $item = $this->getItem();
            $info = ["iconClassChapter" => "text", "iconClassItem" => "fa-file-text-o", "partialView" => "personal-office/item/article/view"];
            if ($item instanceof Article) {
                $info = ["iconClassChapter" => "text", "iconClassItem" => "fa-file-text-o", "partialView" => "personal-office/item/article/view"];
            } elseif ($item instanceof Quiz) {
                $info = ["iconClassChapter" => "final", "iconClassItem" => "fa-question-circle-o", "partialView" => "personal-office/item/quiz/view"];
            } elseif ($item instanceof Video) {
                $info = ["iconClassChapter" => "video", "iconClassItem" => "fa-file-video-o", "partialView" => "personal-office/item/video/view"];
            }
        }

        return $info;
    }

    /**
     * @return \Application\Entity\Course\Item\Quiz\QuizVO
     */
    public function getQuizData()
    {
        return $this->getItemService()->getQuizData($this->getItem()->getIdentity());
    }

    /**
     * Get s3 files preview info for bootstrap fileInput plugin (video, audio)
     *
     * @param \Application\Form\Course\Item\ItemAbstractForm $form
     * @return array
     */
    public function getItemPreviewS3Files($form)
    {
        $item = $this->getItem();
        $previewArr = ['video' => [], 'audio' => [], 'image' => [], 'document' => []];
        $previewArr['video']['src'] = ($item instanceof \Application\Entity\Course\Item\Video && $form->get('video')->getValue()) ? $this->getView()->s3File($item) : "";
        $previewArr['audio']['src'] = ($item instanceof \Application\Entity\Course\Item\Audio && $form->get('audio')->getValue()) ? $this->getView()->s3File($item) : "";
        $previewArr['image']['src'] = ($item instanceof \Application\Entity\Course\Item\Image && $form->get('image')->getValue()) ? $this->getView()->s3File($item) : "";
        $previewArr['document']['src'] = ($item instanceof \Application\Entity\Course\Item\Document && $form->get('document')->getValue()) ? $this->getView()->s3File($item) : "";

        foreach ($previewArr as $type => $params) {
            $fileSrc = $params['src'];
            if ($fileSrc) {
                $mimeType = "";
                $size = "";

                $initial_preview = rawurldecode($fileSrc);

                $previewArr[$type]['mimeType'] = $mimeType;
                $previewArr[$type]['size'] = $size;
                $previewArr[$type]['initialPreview'] = $initial_preview;
            }
        }
        return $previewArr;
    }
}
