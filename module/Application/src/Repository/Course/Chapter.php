<?php
namespace Application\Repository\Course;

use Doctrine\ORM\EntityRepository;

/**
 * Class Chapter
 * Chapter repository
 *
 * @author Paladi Vitalie
 */
class Chapter extends EntityRepository
{
    /**
     * @return QueryBuilder
     */
    public function createBaseQuery()
    {
        $queryBuilder = $this->createQueryBuilder('c');
        return $queryBuilder;
    }

    /**
     * @param $courseId
     * @return \Application\Entity\Course\Chapter[]
     */
    public function getAllChaptersByCourse($courseId)
    {
        $queryBuilder = $this->createBaseQuery();
        $queryBuilder->innerJoin('c.course', 'c2')
            ->andWhere('c2.identity = :c_id')
            ->orderBy('c.position', 'ASC')
            ->setParameter('c_id', $courseId);

        return $queryBuilder->getQuery()->getResult();
    }
}
