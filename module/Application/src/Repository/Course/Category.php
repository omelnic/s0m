<?php
namespace Application\Repository\Course;

use Doctrine\ORM\EntityRepository;

/**
 * Class Category
 * Course category repository
 *
 * @author Paladi Vitalie
 */
class Category extends EntityRepository
{
    /**
     * @return QueryBuilder
     */
    public function createBaseQuery()
    {
        $queryBuilder = $this->createQueryBuilder('c');
        return $queryBuilder;
    }
}
