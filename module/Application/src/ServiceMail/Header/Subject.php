<?php
/**
 * @author Oleg Melnic
 */
namespace Application\ServiceMail\Header;

use Zend\Stdlib\ErrorHandler;

class Subject extends \Zend\Mail\Header\Subject
{
    public function setSubject($subject)
    {
        ErrorHandler::start(E_ALL);

        try {
            return parent::setSubject($subject);
        } catch (\Zend\Mail\Header\Exception\InvalidArgumentException $exception) {
            $error = ErrorHandler::stop();
            if ($error) {
                if (mb_encode_mimeheader($subject, 'utf8', 'Q') === false) {
                    throw $exception;
                }
                $this->subject = $subject;
                $this->encoding = 'UTF-8';
            } else {
                throw $exception;
            }
        } finally {
            ErrorHandler::stop();
        }
        return $this;
    }
}
