<?php
/**
 * @author Paladi Vitalie
 */

namespace Application\Validator;

use Zend\Validator\File\FilesSize;

class ImageFileSize extends FilesSize
{
    public function isValid($value)
    {
       return parent::isValid($value);
    }
}
