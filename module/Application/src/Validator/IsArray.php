<?php
/**
 * @author Melnic Oleg
 */

namespace Application\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class IsArray extends AbstractValidator
{
    const INVALID_TYPE = 'invalid_type';

    protected $messageTemplates = array(
        self::INVALID_TYPE => "You can only use array",

    );

    public function isValid($value)
    {
        if (!is_array($value)) {
            $this->error(self::INVALID_TYPE);
            return false;
        }
        return true;
    }
}
