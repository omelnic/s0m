<?php
namespace Application\Entity\Exception;

use Application\Entity\Exception\ExceptionInterface;

/**
 * class for entities invalid argument exceptions 
 */
class InvalidArgument extends \InvalidArgumentException implements ExceptionInterface
{
}
