<?php

namespace Application\Navigation\Menu;

use Application\Entity\Course\Course;
use Application\Service\Course\Chapter;
use Application\Service\Course\Item\Item;

/**
 * Class Help
 * @package Application\Navigation\Menu
 */
class ProfessorLeft extends AbstractMenu
{
    /**
     * @var Course Сourse
     */
    private $course;

    /**
     * @var Chapter Сourse
     */
    private $serviceChapter;

    /**
     * @var Item Сourse
     */
    private $serviceItem;

    /** @var \Application\Service\Course\Course $courseService */
    protected $courseService;

    /**
     * Подготовить конфигурацию меню навигации
     * @return array
     */
    protected function prepareConfiguration()
    {
        $pages = [];
        $allChapters = $this->getServiceChapter()->getAllChaptersByCourse($this->getCourse());

        if ($allChapters) {
            /** @var \Application\Entity\Course\Chapter $chapter */
            foreach ($allChapters as $chapter) {
                $pages[$chapter->getAlias()] = [
                    'id' => $chapter->getAlias(),
                    'label' => $chapter->getName(),
                    'route' => 'personal-office/chapter/update',
                    'chapterId' => $this->getCourse()->getIdentity(),
                    'params' => [
                        'id' => $chapter->getIdentity(),
                    ],
                    'pages' => $this->getItems($chapter),
                ];
            }
        }

        return $pages;
    }

    private function getItems($chapter)
    {
        $pages = [];
        $allItems = $this->getServiceItem()->getAllItemsByChapter($chapter);

        if ($allItems) {
            /** @var \Application\Entity\Course\Item\ItemAbstract $chapter */
            foreach ($allItems as $item) {
                $pages[$item->getAlias()] = [
                    'id' => $item->getAlias(),
                    'label' => $item->getName(),
                    'route' => 'personal-office/item/update',
                    'params' => [
                        'id' => $item->getIdentity(),
                    ],
                ];
            }
        }

        return $pages;
    }

    /**
     * @return Item
     */
    public function getServiceItem()
    {
        return $this->serviceItem;
    }

    /**
     * @param Item $serviceItem
     */
    public function setServiceItem(Item $serviceItem)
    {
        $this->serviceItem = $serviceItem;
    }

    /**
     * @return Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param Course $course
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;
    }

    /**
     * @return Chapter
     */
    public function getServiceChapter()
    {
        return $this->serviceChapter;
    }

    /**
     * @param Chapter $serviceChapter
     */
    public function setServiceChapter(Chapter $serviceChapter)
    {
        $this->serviceChapter = $serviceChapter;
    }
}
