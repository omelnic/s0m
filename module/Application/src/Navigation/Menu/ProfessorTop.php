<?php

namespace Application\Navigation\Menu;

use Application\Entity\Course\Course;

/**
 * Class Help
 * @package Application\Navigation\Menu
 */
class ProfessorTop extends AbstractMenu
{
    /**
     * @var Course Сourse
     */
    private $course;

    /** @var  \Application\Service\Course\Course */
    protected $courseService;

    /**
     * Подготовить конфигурацию меню навигации
     * @return array
     */
    protected function prepareConfiguration()
    {
        return [
            /*'general'   => [
                'id' => 'simple',
                'label' => 'GENERAL',
                'route'   => 'frontend/subscription_plans',
            ],*/
            'course_info'   => [
                'id' => 'info',
                'label' => 'COURSE INFO',
                'route' => 'personal-office/course/info',
                'params' => [
                    'id' => $this->getCourse()->getIdentity(),
                ],
            ],
            'contents'   => [
                'id' => 'contents',
                'label' => 'CONTENTS',
                'route' => 'personal-office/chapter/create',
                'params' => [
                    'id' => $this->getCourse()->getIdentity(),
                ],
            ],
            /*'rewiews'   => [
                'id' => 'simple',
                'label' => 'REVIEWS',
                'route'   => 'frontend/subscription_plans',
            ],
            'discussions'   => [
                'id' => 'simple',
                'label' => 'DISCUSSIONS',
                'route'   => 'frontend/subscription_plans',
            ],*/
        ];
    }

    /**
     * @return Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param Course $course
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;
    }
}
