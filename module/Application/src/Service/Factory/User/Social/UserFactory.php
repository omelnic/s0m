<?php

namespace Application\Service\Factory\User\Social;

use Application\Service\Mail\User\Social\MailService;
use Application\Service\User\Social\Social;
use Application\Service\User\Social\User;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;
use Application\Service\User\User as ClientService;

/**
 * Class UserFactory
 *
 * @author Palavi Vitalie
 */
class UserFactory implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var User $socialUserService */
        $socialUserService = $callback();
        $socialUserService->setMailService($container->get(MailService::class));
        $socialUserService->setClientService($container->get(ClientService::class));
        $socialUserService->setSocialService($container->get(Social::class));

        return $socialUserService;
    }
}
