<?php
/**
 * @author Олег Мельник
 */

namespace Application\Service\User\Strategy;

use Application\Service\User\User;

/**
 * Class Admin
 *
 * @package Application\Service\User
 *
 * @method \Application\Repository\User\Admin getRepository()
 */
class Admin extends UserAbstract
{
    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\User\Admin::class;
    }

    /**
     * @param array $data
     *
     * @return \Application\Entity\User\Admin
     */
    public function createEmptyEntity(array $data)
    {
        $entity = new \Application\Entity\User\Admin();
        $entity->setRoleProvider($this->getRoleProvider());

        return $entity;
    }

    /**
     * @return \Application\Entity\User\Admin[]|null
     *
     */
    public function getAll()
    {
        return $this->getRepository()->findAll(
            [
                'type' => User::TYPE_ADMIN,
            ]
        );
    }
}
