<?php

namespace Application\Service\Subscription;

use S0mWeb\WTL\Crud\CrudInterface;
use S0mWeb\WTL\Crud\CrudTrait;
use S0mWeb\WTL\Crud\NoInheritanceAwareInterface;
use S0mWeb\WTL\Crud\NoInheritanceAwareTrait;
use CirclicalUser\Service\AuthenticationService;

/**
 * Class Payment
 */
class HistorySubscriptionPackage implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Subscription\ActiveSubscriptionPackage::class;
    }

    /**
     * @param array $data
     * @return object
     */
    public function createEmptyEntity(array $data)
    {
        return new \Application\Entity\Subscription\ActiveSubscriptionPackage();
    }
}
