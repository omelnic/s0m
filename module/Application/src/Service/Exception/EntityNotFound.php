<?php
/**
 * @author Шахновский Александр
 */
namespace Application\Service\Exception;

/**
 * Class EntityNotFound
 */
class EntityNotFound extends \InvalidArgumentException implements ExceptionInterface
{
}
