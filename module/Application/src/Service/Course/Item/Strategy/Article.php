<?php
namespace Application\Service\Course\Item\Strategy;

use S0mWeb\WTL\Crud\CrudInterface;
use S0mWeb\WTL\Crud\CrudTrait;
use S0mWeb\WTL\Crud\NoInheritanceAwareInterface;
use S0mWeb\WTL\Crud\NoInheritanceAwareTrait;

/**
 * Class Article
 *
 * @author Shahnovsky Alex
 */
class Article implements CrudInterface, NoInheritanceAwareInterface
{
    use CrudTrait;
    use NoInheritanceAwareTrait;

    /**
     * @param array $data
     *
     * @return \Application\Entity\Course\Item\Article
     */
    public function createEmptyEntity(array $data)
    {
        return new \Application\Entity\Course\Item\Article();
    }

    /**
     * Получить имя сущности
     *
     * @return string
     */
    public function getBaseEntityName()
    {
        return \Application\Entity\Course\Item\Article::class;
    }
}
