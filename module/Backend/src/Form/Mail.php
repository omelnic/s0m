<?php

namespace Backend\Form;

use S0mWeb\WTL\Form\Form;
use S0mWeb\WTL\StdLib\EntityManagerAwareInterface;
use S0mWeb\WTL\StdLib\EntityManagerAwareTrait;

/**
 * Class Mail
 *
 * @copyright (c) 2017, S0m
 */
class Mail extends Form implements EntityManagerAwareInterface
{
    use EntityManagerAwareTrait;

    public function __construct()
    {
        parent::__construct('mail-form');
        $this->setAttribute('method', 'post');
        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type' => 'textarea',
            'name' => 'mail-body',
            'attributes' => [
                'id' => 'content',
                'class'=>'materialize-textarea',
            ],
            'options' => [
                'label' => 'Mail Body',
            ]
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Send',
                'id' => 'submitbutton',
            ],
        ]);
    }

    /**
     * Get Input Filter name to find it via container
     *
     * @return string Input Filter Name
     */
    public function getInputFilterName()
    {
        return 'Application\InputFilter\Form\SendMail';
    }
}
