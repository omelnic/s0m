<?php
namespace Frontend\Factory\Form\Feedback;

use Frontend\Form\Feedback\Feedback;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\DelegatorFactoryInterface;

/**
 * Class CaptchaDelegator
 *
 * @author Shahnovsky Alex
 */
class CaptchaDelegator implements DelegatorFactoryInterface
{
    /**
     * A factory that creates delegates of a given service
     *
     * @param  ContainerInterface $container
     * @param  string $name
     * @param  callable $callback
     * @param  null|array $options
     *
     * @return Feedback
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var Feedback $form */
        $form = $callback();
        $form->setCaptchaAdapter($container->get('captcha.adapter'));
        $form->addElements();

        return $form;
    }
}
