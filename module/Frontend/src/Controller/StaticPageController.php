<?php
namespace Frontend\Controller;

use Application\Controller\AbstractActionController;
use S0mWeb\WTL\StdLib\ServiceLocatorAwareInterface;
use S0mWeb\WTL\StdLib\ServiceLocatorAwareTrait;
use Zend\View\Model\ViewModel;

/**
 * Class StaticPageController
 *
 * @author Shahnovsky Alex
 */
class StaticPageController extends AbstractActionController implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function indexAction()
    {
        $pageAlias = $this->params()->fromRoute('alias');
        /** @var \StaticPages\Service\Page $service */
        $service = $this->getServiceLocator()->get('StaticPages\Service\Page');
        $page = $service->getByAlias($pageAlias);
        if (is_null($page)) {
            return $this->notFoundAction();
        }

        //todo Integrate dynamic menu
//        if ($page->getGroup()->getGroupObject()->hasMenu()) {
//            $this->leftMenu($page->getGroup()->getGroupObject()->getNavigationKey());
//        }
        $view = new ViewModel([
            'page' => $page,
        ]);

        if ($page->getTemplate()->isFullPage()) {
            $view->setTemplate('frontend/static-page/full-page');
        }

        return $view;
    }
}
