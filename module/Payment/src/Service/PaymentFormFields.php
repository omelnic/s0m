<?php

namespace Payment\Service;

/**
 * Class PaymentFormConfig
 * @author nonick <web@nonick.name>
 */
class PaymentFormFields
{
    /** @var  string $accessKey */
    private $accessKey;
    /** @var  string $profileId */
    private $profileId;
    /** @var  string $secureAlgorithm */
    private $secureAlgorithm;
    /** @var  string $secretKey */
    private $secretKey;
    /** @var  string $endpoint */
    private $endpoint;

    /**
     * @return string
     */
    public function getAccessKey()
    {
        return $this->accessKey;
    }

    /**
     * @param string $accessKey
     */
    public function setAccessKey($accessKey)
    {
        $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * @param string $profileId
     */
    public function setProfileId($profileId)
    {
        $this->profileId = $profileId;
    }

    /**
     * @return string
     */
    public function getSecureAlgorithm()
    {
        return $this->secureAlgorithm;
    }

    /**
     * @param string $secureAlgorithm
     */
    public function setSecureAlgorithm($secureAlgorithm)
    {
        $this->secureAlgorithm = $secureAlgorithm;
    }

    /**
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * @param string $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function getSignature(array $data)
    {
        return base64_encode(hash_hmac($this->getSecureAlgorithm(), $this->getDataToSign($data), $this->getSecretKey(), true));
    }

    /**
     * @return string
     */
    public function getSignedFields()
    {
        $fields = [
            'access_key',
            'profile_id',
            'transaction_uuid',
            'signed_field_names',
            'unsigned_field_names',
            'signed_date_time',
            'locale',
            'transaction_type',
            'reference_number',
            'amount,currency',
        ];

        return implode(',', $fields);
    }

    /**
     * Собрать строку для подписи из полей формы
     * @param $data
     * @return string
     * @throws \Exception
     */
    private function getDataToSign($data)
    {
        if (!isset($data['signed_field_names'])) {
            throw new \Exception('Invalid form');
        }

        $signedFields = $data['signed_field_names'];
        $signedFieldNames = explode(",", $signedFields);
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . "=" . $data[$field];
        }
        return implode(",", $dataToSign);
    }
}
