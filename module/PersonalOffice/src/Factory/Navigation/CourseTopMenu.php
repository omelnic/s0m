<?php
/**
 * @author Oleg Melnic
 */

namespace PersonalOffice\Factory\Navigation;

use Application\Navigation\Menu\Course;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class HelpLeftMenu
 * @package FrontEnd\Factory\Navigation
 */
class CourseTopMenu implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new Course();
        return $service($container, \Zend\Navigation\Navigation::class);
    }
}
