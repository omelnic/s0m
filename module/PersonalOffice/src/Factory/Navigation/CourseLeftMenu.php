<?php
/**
 * @author Vitalie Paladi
 */

namespace PersonalOffice\Factory\Navigation;

use Application\Navigation\Menu\CourseLeft;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Course\Chapter as ChapterService;
use Application\Service\Course\Item\Item as ItemService;

/**
 * Class CourseLeftMenu
 */
class CourseLeftMenu implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CourseLeft();
        $service->setChapterService($container->get(ChapterService::class));
        $service->setItemService($container->get(ItemService::class));
        if (isset($options['course'])) {
            $service->setCourse($options['course']);
        }

        return $service($container, \Zend\Navigation\Navigation::class);
    }
}
