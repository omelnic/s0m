<?php
/**
 * @author Oleg Melnic
 */

namespace PersonalOffice\Factory\Navigation;

use Application\Factory\Exception\InvalidArgument;
use Application\Navigation\Menu\ProfessorTop;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class HelpLeftMenu
 * @package FrontEnd\Factory\Navigation
 */
class ProfessorTopMenu implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     * @return mixed
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ProfessorTop();

        if (isset($options['course']) && $options['course'] instanceof \Application\Entity\Course\Course) {
            $service->setCourse($options['course']);
        } else {
            throw new InvalidArgument(
                'Course param must be set and implement "\Application\Entity\Course\Course"'
            );
        }

        return $service($container, \Zend\Navigation\Navigation::class);
    }
}
