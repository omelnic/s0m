<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace PersonalOffice;

use Frontend\Controller\UserController;
use PersonalOffice\Controller\IndexController;
use Zend\Router\Http\Literal;
use Zend\Router\Http\TreeRouteStack;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return array(
    'router' => array(
        'router_class' => TreeRouteStack::class,
        'routes' => array(
            'personal-office' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/personal-office/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                    'route_plugins' => array(),
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'subscription' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'profile-plans/',
                            'defaults' => [
                                'controller' => Controller\SubscriptionController::class,
                                'action' => 'profilePlans',
                            ],
                        ],
                    ],
                    'offline-event' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'offline-event/[:alias/]',
                            'defaults' => [
                                'controller' => Controller\OfflineEventController::class,
                                'action' => 'item',
                            ]
                        ]
                    ],
                    'subscribe-offline-event' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'subscribe-offline-event/[:id/]',
                            'defaults' => [
                                'controller' => Controller\OfflineEventController::class,
                                'action' => 'subscribe',
                            ]
                        ]
                    ],
                    'unsubscribe-offline-event' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'unsubscribe-offline-event/[:id/]',
                            'defaults' => [
                                'controller' => Controller\OfflineEventController::class,
                                'action' => 'unsubscribe',
                            ]
                        ]
                    ],
                    'offline-events' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'offline-events/',
                            'defaults' => [
                                'controller' => Controller\OfflineEventController::class,
                                'action' => 'list',
                            ]
                        ]
                    ],
                    'course' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'course/',
                            'defaults' => [
                                'controller' => Controller\CourseController::class,
                                'action' => 'item',
                            ]
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'single' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => ':alias/',
                                    'defaults' => [
                                        'controller' => Controller\CourseController::class,
                                        'action' => 'courseInfo',
                                    ]
                                ]
                            ],
                            'index' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => 'courses/',
                                    'defaults' => [
                                        'controller' => Controller\CourseController::class,
                                        'action' => 'index',
                                    ]
                                ]
                            ],
                            'info' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'info/:id/',
                                    'defaults' => [
                                        'controller' => Controller\CourseController::class,
                                        'action' => 'courseInfo',
                                    ]
                                ]
                            ],
                            'create' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => 'create/',
                                    'defaults' => [
                                        'controller' => Controller\CourseController::class,
                                        'action' => 'courseCreate',
                                    ]
                                ]
                            ],
                            'my-courses' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'my/',
                                    'defaults' => [
                                        'controller' => Controller\CourseController::class,
                                        'action' => 'myCourses',
                                    ]
                                ]
                            ],
                        ],
                    ],
                    'chapter' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => 'chapter/[:alias/]',
                            'defaults' => [
                                'controller' => Controller\ChapterController::class,
                                'action' => 'item',
                            ]
                        ]
                    ],
                    'contents' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'contents/',
                            'defaults' => [
                                'controller' => Controller\ChapterController::class,
                                'action' => 'contents',
                            ]
                        ]
                    ],
                    'chapter' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'chapter/',
                            'defaults' => [
                                'controller' => Controller\ChapterController::class,
                                'action' => 'create',
                            ]
                        ],
                        'child_routes' => [
                            'create' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'create/:id/',
                                    'defaults' => [
                                        'controller' => Controller\ChapterController::class,
                                        'action' => 'create',
                                    ]
                                ]
                            ],
                            'update' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'update/:id/',
                                    'defaults' => [
                                        'controller' => Controller\ChapterController::class,
                                        'action' => 'update',
                                    ]
                                ]
                            ],
                            'delete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'delete/:id/',
                                    'defaults' => [
                                        'controller' => Controller\ChapterController::class,
                                        'action' => 'delete',
                                    ]
                                ]
                            ],
                            'sort' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'sort/:id/',
                                    'defaults' => [
                                        'controller' => Controller\ChapterController::class,
                                        'action' => 'sort',
                                    ]
                                ]
                            ],
                        ],
                    ],
                    'item' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'item/',
                            'defaults' => [
                                'controller' => Controller\ItemController::class,
                                'action' => 'create',
                            ]
                        ],
                        'child_routes' => [
                            'create' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'create/:id/',
                                    'defaults' => [
                                        'controller' => Controller\ItemController::class,
                                        'action' => 'create',
                                    ]
                                ]
                            ],
                            'update' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => 'update/:id/',
                                    'defaults' => [
                                        'controller' => Controller\ItemController::class,
                                        'action' => 'update',
                                    ]
                                ]
                            ],
                        ],
                    ],
                    'personal-data' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'personal-data/',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'personalData',
                            ]
                        ]
                    ],
                    'change-password' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'change-password/',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'changePassword',
                            ]
                        ]
                    ],
                    'identity-verification' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'identity-verification/',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'identityVerification',
                            ]
                        ]
                    ],
                    'linked-accounts' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'linked-accounts/',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'linkedAccounts',
                            ]
                        ]
                    ],
                    'subscription-plan' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => 'subscription-plan/',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'subscriptionPlan',
                            ]
                        ]
                    ],
                    'delete-account' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => 'delete-account/',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller'    => Controller\ProfileController::class,
                                'action'        => 'deleteAccount',
                            ],
                        ],
                    ],
                ]
            ],
        ),
    ),
    'controllers' => array(
        'factories' => array(
            Controller\IndexController::class => InvokableFactory::class,
            Controller\SubscriptionController::class => InvokableFactory::class,
            Controller\OfflineEventController::class => InvokableFactory::class,
            Controller\CourseController::class => InvokableFactory::class,
            Controller\ChapterController::class => InvokableFactory::class,
            Controller\ItemController::class => InvokableFactory::class,
            Controller\ProfileController::class => InvokableFactory::class,
        ),
    ),
    'controller_plugins' => [
        'factories' => [
            'topMenu' => \PersonalOffice\Factory\Controller\Plugin\TopMenu::class,
            'personalOfficeMenu' => \PersonalOffice\Factory\Controller\Plugin\PersonalOfficeMenu::class,
            'personalOfficeLeftMenu' => \PersonalOffice\Factory\Controller\Plugin\PersonalOfficeLeftMenu::class,
            'leftMenu' => \PersonalOffice\Factory\Controller\Plugin\LeftMenu::class,
        ]
    ],
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => [
            'error/404' => __DIR__.'/../../Frontend/view/error/404.phtml',
            'error/index' => __DIR__.'/../../Frontend/view/error/index.phtml',
            'personal-office/index/index' => __DIR__.'/../view/personal-office/index/index.phtml',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ),
    'view_helpers' => array(
        'invokables' => array(
        ),
        'factories' => array(
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'circlical' => [
        'user' => [
            'guards' => [
                'PersonalOffice' => [
                    "controllers" => [
                        \PersonalOffice\Controller\IndexController::class => [
                            'default' => [
                                    'professor',
                                    'student',
                                ], // anyone can access
                        ],
                        \PersonalOffice\Controller\SubscriptionController::class => [
                            'default' => [
                                    'professor',
                                    'student',
                                ], // anyone can access
                        ],
                        \PersonalOffice\Controller\OfflineEventController::class => [
                            'default' => [
                                    'professor',
                                    'student',
                                ], // anyone can access
                        ],
                        \PersonalOffice\Controller\CourseController::class => [
                            'default' => [
                                'professor',
                                'student',
                            ],
                        ],
                        \PersonalOffice\Controller\ChapterController::class => [
                            'default' => [
                                'professor',
                                'student',
                            ], // anyone can access
                        ],
                        \PersonalOffice\Controller\ProfileController::class => [
                            'actions' => [
                                'update' => [
                                    'professor',
                                ],
                                'create' => [
                                    'professor',
                                ],
                                'personalData' => [
                                    'professor',
                                    'student',
                                ],
                                'changePassword' => [
                                    'professor',
                                    'student',
                                ],
                                'linkedAccounts' => [
                                    'professor',
                                    'student',
                                ],
                                'deleteAccount' => [
                                    'professor',
                                    'student',
                                ],
                            ],
                        ],
                        \PersonalOffice\Controller\ItemController::class => [
                            'default' => [
                                'professor',
                            ],
                        ],
                    ],
                ],
            ],
            'deny_strategy' => [
                'class' => \CirclicalUser\Strategy\RedirectStrategy::class,
                'options' => [
                    'controller' => UserController::class,
                    'action' => 'login',
                ],
            ],
        ],
    ],
);
