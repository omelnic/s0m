<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace S0mApi;

use S0mApi\Service\AWS\Polly\Polly;
use S0mApi\Service\AWS\S3\S3;
use S0mApi\Service\TextToVoiceInterface;
use S0mApi\Service\Watson\TextToVoice;
use S0mApi\Service\Watson\VoiceToText;
use S0mApi\Service\Zeroth\Zeroth;
use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            's0m-api-voice-to-text' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/voiceToText',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'voiceToText',
                    ],
                    'route_plugins' => [],
                ],
                'may_terminate' => true,
            ],
            's0m-api-ask-ai' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api/askAI',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'askAI',
                    ],
                    'route_plugins' => [],
                ],
                'may_terminate' => true,
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            \S0mApi\Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'service_manager' => [
        'initializers' => [],
        'abstract_factories' => [
        ],
        'aliases' => [
            TextToVoiceInterface::class => TextToVoice::class,
        ],
        'factories' => [
            TextToVoice::class => \S0mApi\Service\Watson\Factory\TextToVoice::class,
            VoiceToText::class => \S0mApi\Service\Watson\Factory\VoiceToText::class,
            Zeroth::class => \S0mApi\Service\Zeroth\Factory\Zeroth::class,
            Polly::class => \S0mApi\Service\AWS\Polly\Factory\Polly::class,
            S3::class => \S0mApi\Service\AWS\S3\Factory\S3::class,
        ],
        'delegators' => [
        ],
        'shared' => [],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'circlical' => [
        'user' => [
            'guards' => [
                'S0mApi' => [
                    "controllers" => [
                        \S0mApi\Controller\IndexController::class => [
                            'default' => [], // anyone can access
                        ],
                    ],
                ],
            ],
        ],
    ],
];
