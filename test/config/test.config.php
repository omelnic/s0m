<?php
return array(
    'module_listener_options' => array(
        'config_glob_paths' => array(
            'base' => 'config/autoload/{,*.}global.php',
            'test' => 'test/config/base/autoload/{,*.}test.php',
        ),
        'config_cache_enabled' => false,
    ),
    'module_paths' => array(
        './test',
    ),
    'service_manager' => array(
        'factories' => array(
            'ServiceListener' => 'FunctionalTest\Framework\ServiceListenerFactory',
        )
    )
);
