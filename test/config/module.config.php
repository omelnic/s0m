<?php
return [
    'modules' => [
        'FunctionalTest'
    ],
    'service_manager' => [
        'factories' => [
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
    ],
];
