<?php
namespace FunctionalTest\Framework;

/**
 * Class EntityInputFilterTestCase
 *
 * @author Shahnovsky Alex
 */
abstract class EntityInputFilterTestCase extends InputFilterBuilderTestCase
{
    /**
     * @return string
     */
    abstract protected function getEntityName();

    /**
     * @return array
     */
    protected function getFilterArray()
    {
        $entity = new \ReflectionClass($this->getEntityName());
        $properties = $entity->getProperties();
        $result = [];
        foreach ($properties as $property) {
            $result[] = $property->getName();
        }

        return $result;
    }

    /**
     * Список внутренних полей сущности, для которых не должно быть фильтров и валидаторов
     * identity - добавится автоматически
     *
     * @return array
     */
    abstract protected function excludeProperties();

    /**
     * @return array
     */
    protected function getExcludeProperties()
    {
        return array_merge(['identity'], $this->excludeProperties());
    }

    public function testCreateFilter()
    {
        $filter = $this->getBuilder()->getInputFilterBuilder()->createFilter();
        $existing = $this->getExistingFilters($filter->getInputs());
        $properties = array_diff($this->getFilterArray(), $this->getExcludeProperties());

        $diff = array_diff($properties, $existing);
        $this->assertEmpty(
            $diff,
            sprintf('Не все поля присутсвуют в фильтрах. %s', print_r($diff, true))
        );
    }
}
