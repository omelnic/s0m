<?php
namespace FunctionalTest\Framework;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\Http\Headers;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Stdlib\Parameters;

/**
 * Class RequestFactory
 *
 * @author Shahnovsky Alex
 */
class RequestFactory implements FactoryInterface
{

    private static $uri;

    private static $method;

    private static $data;

    private static $headers;

    public static function requestConfig($uri, $method, $data, Headers $headers = null)
    {
        self::$uri = $uri;
        self::$method = $method;
        self::$data = $data;
        self::$headers = $headers;
    }

    /**
     * @return Request
     */
    protected function createClearRequest()
    {
        $request = $this->createRequest();
        $request->setEnv(new Parameters())
            ->setQuery(new Parameters())
            ->setPost(new Parameters())
            ->setFiles(new Parameters())
            ->setHeaders(new Headers());

        return $request;
    }

    protected function createRequest()
    {
        return new Request();
    }

    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (is_null(self::$uri)) {
            return $this->createRequest();
        }

        $request = $this->createClearRequest();

        foreach (self::$data as $key => $value) {
            $method = 'set'.ucfirst($key);
            $request->$method(new Parameters($value));
        }

        if (!is_null(self::$headers)) {
            $request->setHeaders(self::$headers);
        }

        $request->setUri(self::$uri);
        $request->setMethod(self::$method);

        return $request;
    }
}
