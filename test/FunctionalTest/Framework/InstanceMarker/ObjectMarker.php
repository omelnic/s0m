<?php
namespace FunctionalTest\Framework\InstanceMarker;

use PHPUnit\Framework\TestCase;

/**
 * Class ObjectMarker
 *
 * @author Shahnovsky Alex
 */
class ObjectMarker extends InstanceMarkerAbstract
{
    /**
     * @var string
     */
    private $instanceName = null;

    public function __construct($instanceName, \Closure $callBack = null)
    {
        $this->setInstanceName($instanceName);
        $this->setCallBack($callBack);
    }

    /**
     * @return null
     */
    public function getInstanceName()
    {
        return $this->instanceName;
    }

    /**
     * @param null $instanceName
     */
    private function setInstanceName($instanceName)
    {
        $this->instanceName = $instanceName;
    }

    protected function assert(TestCase $phpunit, $value, $message)
    {
        $phpunit->assertInstanceOf($this->getInstanceName(), $value, $message);
    }
}
