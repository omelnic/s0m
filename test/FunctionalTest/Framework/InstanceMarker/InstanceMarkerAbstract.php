<?php
namespace FunctionalTest\Framework\InstanceMarker;

use PHPUnit\Framework\TestCase;

/**
 * Class InstanceMarkerAbstract
 *
 * @author Shahnovsky Alex
 */
abstract class InstanceMarkerAbstract implements InstanceMarkerInterface
{
    /**
     * @var \Closure
     */
    protected $callBack;

    public function apply(TestCase $phpunit, $value, $message)
    {
        $this->assert($phpunit, $value, $message);
        if ($this->hasCallBack()) {
            $c = $this->getCallBack();
            $c($value, $phpunit);
        }
    }

    /**
     * @return \Closure
     */
    protected function getCallBack()
    {
        return $this->callBack;
    }

    protected function hasCallBack()
    {
        return !is_null($this->callBack);
    }

    /**
     * @param \Closure $callBack
     */
    protected function setCallBack(\Closure $callBack = null)
    {
        $this->callBack = $callBack;
    }

    abstract protected function assert(TestCase $phpunit, $value, $message);
}
