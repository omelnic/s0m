<?php
namespace FunctionalTest\Framework\InstanceMarker;

use PHPUnit\Framework\TestCase;

/**
 * Class DateString
 *
 * @author Shahnovsky Alex
 */
class DateString extends InstanceMarkerAbstract
{
    protected $format;
    protected $allowEmpty;

    public function __construct($format, $allowEmpty = false, \Closure $callback = null)
    {
        $this->format = $format;
        $this->allowEmpty = $allowEmpty;
        $this->setCallBack($callback);
    }

    protected function assert(TestCase $phpunit, $value, $message)
    {
        if ($value === null && !$this->allowEmpty) {
            $phpunit->fail($message);
        }

        if ($value !== null) {
            $phpunit->assertNotFalse(\DateTime::createFromFormat($this->format, $value), $message);
        }
    }
}
