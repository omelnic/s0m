<?php
namespace FunctionalTest\Framework\Authentication;

use CirclicalUser\Provider\RoleInterface;
use CirclicalUser\Provider\RoleProviderInterface;
use CirclicalUser\Provider\UserInterface;

/**
 * Class UserMock
 *
 * @author Shahnovsky Alex
 */
class UserMock implements UserInterface
{
    /**
     * @var RoleProviderInterface
     */
    private $roleProvider;

    /**
     * @var array
     */
    private $roleNames;

    /**
     * @var \CirclicalUser\Provider\RoleInterface[]
     */
    private $userRoles;

    /**
     * UserMock constructor.
     *
     * @param RoleProviderInterface $roleProvider
     * @param array $roleNames
     */
    public function __construct(RoleProviderInterface $roleProvider, $roleNames)
    {
        $this->roleProvider = $roleProvider;
        $this->roleNames = $roleNames;
    }

    public function getId()
    {
        return 999;
    }

    /**
     * @return \CirclicalUser\Provider\RoleInterface[]
     */
    public function getRoles()
    {
        if (is_null($this->userRoles)) {
            $this->initRoles();
        }

        return $this->userRoles;
    }

    /**
     * Get role entities by names
     *
     * @return void
     */
    private function initRoles() {
        foreach ($this->roleNames as $name) {
            $this->userRoles[] = $this->roleProvider->getRoleWithName($name);
        }
    }

    public function getEmail()
    {
        return 'test@test.com';
    }

    public function addRole(RoleInterface $role)
    {
    }
}
