<?php
namespace FunctionalTest\Framework\Authentication;

use CirclicalUser\Mapper\AuthenticationMapper;
use CirclicalUser\Mapper\UserMapper;
use CirclicalUser\Provider\PasswordCheckerInterface;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class AuthServiceFactory
 *
 * @author Shahnovsky Alex
 */
class AuthServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string $requestedName
     * @param  null|array $options
     *
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $userConfig = $config['circlical']['user'];

        $userProvider = $userConfig['providers']['user'] ?? UserMapper::class;
        $authMapper = $userConfig['providers']['auth'] ?? AuthenticationMapper::class;
        $passwordChecker = null;

        if( !empty( $userConfig['password_strength_checker'] ) ){
            $checkerImplementation = new $userConfig['password_strength_checker'];
            if( $checkerImplementation instanceof PasswordCheckerInterface ){
                $passwordChecker = $checkerImplementation;
            }
        }
        $authProvider = $container->get($authMapper);
        $service = new \FunctionalTest\Framework\Authentication\AuthService(
            $authProvider,
            $container->get($userProvider),
            base64_decode($userConfig['auth']['crypto_key']),
            $userConfig['auth']['transient'],
            false,
            $passwordChecker
        );
        $service->setAuthProvider($authProvider);

        return $service;
    }
}
