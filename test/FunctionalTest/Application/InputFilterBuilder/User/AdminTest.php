<?php
namespace FunctionalTest\Application\InputFilterBuilder\User;

use Application\Entity\User\Admin;
use Application\Entity\User\State;
use FunctionalTest\Framework\EntityInputFilterTestCase;
use FunctionalTest\Framework\InstanceMarker\ObjectMarker;

/**
 * Class AdminTest
 *
 * @author Shahnovsky Alex
 */
class AdminTest extends EntityInputFilterTestCase
{
    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Admin::class;
    }

    /**
     * Список внутренних полей сущности, для которых не должно быть фильтров и валидаторов
     * identity - добавится автоматически
     *
     * @return array
     */
    protected function excludeProperties()
    {
        return [
            'passwordResetToken',
            'passwordResetTokenCreationDate',
        ];
    }

    /**
     * @return string
     */
    protected function getBuilderName()
    {
        return \Application\InputFilterBuilder\User\Strategy\Admin::class;
    }

    /**
     * Массив валидных данных и ожидаемых данных после фильтрации
     *
     * пример:
     * array(
     *  'data' => array(
     *    'alias' => 'valid-alias',
     *    'user' => 1
     *  ),
     *  'expected' => array(
     *    'alias' => 'valid-alias',
     *    'user' => new InstanceMarker('Common\Entity\User')
     *  ),
     * 'fixtures' => array() - если нужны
     * )
     *
     * @return array
     */
    public function dataValid()
    {
        return [
            [
                'data' => [
                    'email' => 'test@test.com',
                    'password' => 'password1234',
                    'state' => State::ACTIVE,
                    'birthday' => '1999-01-01',
                ],
                'expected' => [
                    'email' => 'test@test.com',
                    'password' => 'password1234',
                    'state' => new ObjectMarker(State::class),
                    'fullName' => null,
                    'birthday' => new ObjectMarker(\DateTime::class),
                    'address' => null,
                    'timezone' => null,
                    'description' => null,
                    'sex' => null,
                    'avatar' => null,
                ],
                'fixtures' => [
                ],
            ],
        ];
    }

    /**
     * Массив не валидных данных
     *
     * пример:
     *
     * array(
     *   'data' => array(
     *     'alias' => 'not valid alias',
     *     'user' => 100500
     *   ),
     *   'messages' => array(
     *     'alias', 'user'
     *   ),
     *   'context' => [],
     *   'fixtures' => [],
     * )
     *
     * @return array
     */
    public function dataNotValid()
    {
        return [
            'empty data' => [
                'data' => [
                ],
                'messages' => [
                    'email',
                    'password',
                    'birthday',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
        ];
    }
}
