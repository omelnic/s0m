<?php
/**
 * Created by Topuria Evgheni.
 * Date: 6/9/17
 */

namespace FunctionalTest\Application\InputFilterBuilder\Subscription;

use Application\Entity\Subscription\SubscriptionPackage;
use Application\Entity\ValueObject\Period;
use FunctionalTest\Framework\EntityInputFilterTestCase;
use FunctionalTest\Framework\InstanceMarker\ObjectMarker;

class SubscriptionPackageTest extends EntityInputFilterTestCase
{
    /**
     * @return string
     */
    protected function getBuilderName()
    {
        return \Application\InputFilterBuilder\Subscription\SubscriptionPackage::class;
    }

    /**
     * Массив валидных данных и ожидаемых данных после фильтрации
     *
     * пример:
     * array(
     *  'data' => array(
     *    'alias' => 'valid-alias',
     *    'user' => 1
     *  ),
     *  'expected' => array(
     *    'alias' => 'valid-alias',
     *    'user' => new InstanceMarker('Common\Entity\User')
     *  ),
     * 'fixtures' => array() - если нужны
     * )
     *
     * @return array
     */
    public function dataValid()
    {
        return [
            [
                'data' => [
                    'name' => 'package1',
                    'price' => '19.99',
                    'period' => Period::PERIOD_ONE_YEAR,
                    'trial' => 1,
                ],
                'expected' => [
                    'name' => 'package1',
                    'price' => '19.99',
                    'period' => new ObjectMarker(Period::class),
                ],
                'fixtures' => [
                ],
            ],
        ];
    }

    /**
     * Массив не валидных данных
     *
     * пример:
     *
     * array(
     *   'data' => array(
     *     'alias' => 'not valid alias',
     *     'user' => 100500
     *   ),
     *   'messages' => array(
     *     'alias', 'user'
     *   ),
     *   'context' => [],
     *   'fixtures' => [],
     * )
     *
     * @return array
     */
    public function dataNotValid()
    {
        return [
            [
                'data' => [
                    'period' => 'string',
                ],
                'messages' => [
                    'name',
                    'price',
                    'period',
                ],
                'context' => [
                ],
                'fixtures' => [
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return SubscriptionPackage::class;
    }

    /**
     * Список внутренних полей сущности, для которых не должно быть фильтров и валидаторов
     * identity - добавится автоматически
     *
     * @return array
     */
    protected function excludeProperties()
    {
        return [];
    }
}