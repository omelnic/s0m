<?php

namespace FunctionalTest\Application\Service\File\Mock;

use Gedmo\Uploadable\FileInfo\FileInfoArray;

class FileInfoMock extends FileInfoArray
{
    public function isUploadedFile()
    {
        return false;
    }
}
