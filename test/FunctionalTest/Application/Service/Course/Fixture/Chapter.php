<?php
namespace FunctionalTest\Application\Service\Course\Fixture;

use Application\Entity\Course\ChapterState;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Chapter
 *
 * @author Oleg Melnic
 */
class Chapter extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $chapter = new \Application\Entity\Course\Chapter();
        $chapter->setName('Chapter name');
        $chapter->setDescription('Chapter description');
        $chapter->setState(ChapterState::createFromScalar(ChapterState::ACTIVE));
        $chapter->setAlias('chapter1');
        $course = $this->getReference('course_fixture');
        $chapter->setCourse($course);
        $this->setReference('course_chapter', $chapter);

        $manager->persist($chapter);
        $manager->flush();
    }
}
