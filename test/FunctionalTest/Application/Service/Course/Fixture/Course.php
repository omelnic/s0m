<?php
namespace FunctionalTest\Application\Service\Course\Fixture;

use Application\Entity\Course\Difficulty;
use Application\Entity\ValueObject\Period;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Course
 *
 * @author Oleg Melnic
 */
class Course extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = $this->getReference('user_admin_fixture');
        $course = new \Application\Entity\Course\Course();
        $course->setName('Course name');
        $course->setDescription('Course description');
        $course->setAlias('course1');
        $course->setAuthor($admin);
        $course->setTeacher($admin);
        $course->setDifficulty(Difficulty::createFromScalar(Difficulty::BEGINNER));
        $course->setExecution(Period::createFromScalar(Period::PERIOD_HALF_YEAR));
        $course->setHowPass('how pass');

        $manager->persist($course);
        $manager->flush();
        $this->addReference('course_fixture', $course);
    }
}
