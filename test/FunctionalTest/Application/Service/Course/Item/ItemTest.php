<?php
namespace FunctionalTest\Application\Service\Course\Item;

use Application\Entity\Course\Item\Article;
use Application\Entity\Course\Item\Audio;
use Application\Entity\Course\Item\Quiz;
use Application\Entity\Course\Item\State;
use Application\Entity\Course\Item\Video;
use Application\Entity\Course\Item\Document;
use Application\Entity\Course\Item\Image;
use Application\Service\Course\Item\Item;
use Doctrine\Common\Collections\ArrayCollection;
use FunctionalTest\Framework\ServiceTestCase;
use PHPUnit\Framework\TestCase;

/**
 * Class ItemTest
 *
 * @author Shahnovsky Alex
 */
class ItemTest extends ServiceTestCase
{

    /**
     * Возвращает имя сервиса в ServiceManager
     *
     * @return string
     */
    protected function getServiceName()
    {
        return Item::class;
    }

    /**
     * Возвращает тип сервиса (имя класса)
     *
     * @return string
     */
    protected function getServiceType()
    {
        return Item::class;
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'data' => [....],
     *      'fixtures' => array()
     *      'entityName' => string,
     *      'callback' => Closure|null
     *      'role' => array|string|null
     * ]
     *
     * @return array
     */
    public function dataCreateSuccess()
    {
        return [
            'validData article' => [
                'data' => [
                    'name' => 'Article 1',
                    'description' => 'some long description',
                    'content' => 'some long content',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'type' => 'article',
                    'chapter' => 1,
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
                'entityName' => Article::class,
            ],
            'validData video' => [
                'data' => [
                    'name' => 'Video 1',
                    'description' => 'some long description',
                    'video' => 'video/8df7b73a7820f4aef47864f2a6c5fccf',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'type' => 'video',
                    'chapter' => 1,
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
                'entityName' => Video::class,
            ],
            'validData audio' => [
                'data' => [
                    'name' => 'Audio 1',
                    'description' => 'some long description',
                    'audio' => 'audio/audio-link',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'type' => 'audio',
                    'chapter' => 1,
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
                'entityName' => Audio::class,
            ],
            'validData quiz' => [
                'data' => [
                    'name' => 'Quiz 1',
                    'description' => 'some long description',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'quiz' => [
                        'questions' => [
                            [
                                'question' => 'Who?',
                                'answers' => [
                                    1 => 'correct answer',
                                    2 => 'wrong answer1',
                                    3 => 'wrong answer2',
                                    4 => 'wrong answer3',
                                ],
                                'correctAnswer' => 1,
                            ],
                        ],
                    ],
                    'type' => 'quiz',
                    'chapter' => 1,
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
                'entityName' => Quiz::class,
            ],
            'validData document' => [
                'data' => [
                    'name' => 'Document 1',
                    'description' => 'some long description',
                    'document' => 'http://youtube.com/document-link',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'type' => 'document',
                    'chapter' => 1,
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
                'entityName' => Document::class,
            ],
            'validData image' => [
                'data' => [
                    'name' => 'Image item 1',
                    'description' => 'some long description',
                    'image' => 'img/8df7b73a7820f4aef47864f2a6c5fccf',
                    'state' => State::ACTIVE,
                    'isFree' => 1,
                    'startDate' => '2015-01-01',
                    'type' => 'image',
                    'chapter' => 1,
                ],
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                ],
                'entityName' => Image::class,
            ],
        ];
    }

    /**
     * dataProvider for testCreateFailed
     *
     * Format: [
     *  'data' => array(invalid data),
     *  'fixtures' => array(),
     *  'messageKeys' => array(список всех ожидаемых ключей)
     * ]
     *
     * @return array
     */
    public function dataCreateFailed()
    {
        return [
            'empty data for article' => [
                'data' => [
                    'type' => 'article',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'content',
                    'chapter',
                ]
            ],
            'empty data for video' => [
                'data' => [
                    'type' => 'video',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'video',
                    'chapter',
                ]
            ],
            'empty data for audio' => [
                'data' => [
                    'type' => 'audio',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'audio',
                    'chapter',
                ]
            ],
            'empty data for quiz' => [
                'data' => [
                    'type' => 'quiz',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'quiz',
                    'chapter',
                ]
            ],
            'empty data for document' => [
                'data' => [
                    'type' => 'document',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'document',
                    'chapter',
                ]
            ],
            'empty data for image' => [
                'data' => [
                    'type' => 'image',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'name',
                    'isFree',
                    'startDate',
                    'state',
                    'image',
                    'chapter',
                ]
            ],
        ];
    }

    /**
     * dataProvider For testUpdateSuccess
     *
     * Format: [
     *  'id' => int,
     *  'data' => array(...),
     *  'entityName' => 'имя entity',
     *  'fixtureName' => array(список классов fixtures),
     *  'expectedChanges' => array(список колонок в бд и их значения)
     *  'callback' => function($updateEntity, $this) {}  - если нужны доп проверки
     * ]
     *
     * @return array
     */
    public function dataUpdateSuccess()
    {
        return [
            'update article content' => [
                'identity' => 1,
                'data' => [
                    'content' => 'new content',
                ],
                'entityName' => Article::class,
                'fixtureName' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Article::class,
                ],
                'expectedChanges' => [
                    'content' => 'new content',
                ]
            ],
            'update video url' => [
                'identity' => 1,
                'data' => [
                    'video' => 'video/8df7b73a7820f4aef47864f2a6c5fccf',
                ],
                'entityName' => Video::class,
                'fixtureName' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Video::class,
                ],
                'expectedChanges' => [
                    'video' => 'video/8df7b73a7820f4aef47864f2a6c5fccf',
                ]
            ],
            'update audio url' => [
                'identity' => 1,
                'data' => [
                    'audio' => 'audio/new-audio-link',
                ],
                'entityName' => Audio::class,
                'fixtureName' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Audio::class,
                ],
                'expectedChanges' => [
                    'audio' => 'audio/new-audio-link',
                ]
            ],
            'update quiz data' => [
                'identity' => 1,
                'data' => [
                    'quiz' => [
                        'questions' => [
                            [
                                'question' => 'Why?',
                                'answers' => [
                                    1 => 'new wrong answer1',
                                    2 => 'new correct answer',
                                    3 => 'new wrong answer2',
                                    4 => 'new wrong answer3',
                                ],
                                'correctAnswer' => 2,
                            ],
                        ],
                    ],
                ],
                'entityName' => Quiz::class,
                'fixtureName' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Quiz::class,
                ],
                'expectedChanges' => [
                    'quiz_data' => serialize(new ArrayCollection(
                        [
                            'questions' => [
                                [
                                    'question' => 'Why?',
                                    'answers' => [
                                        1 => 'new wrong answer1',
                                        2 => 'new correct answer',
                                        3 => 'new wrong answer2',
                                        4 => 'new wrong answer3',
                                    ],
                                    'correctAnswer' => 2,
                                ],
                            ],
                        ]
                    )),
                ],
                'callback' => function (Quiz $quiz, TestCase $phpunit) {
                    $phpunit->assertSame(
                        [
                            'questions' => [
                                [
                                    'question' => 'Why?',
                                    'answers' => [
                                        1 => 'new wrong answer1',
                                        2 => 'new correct answer',
                                        3 => 'new wrong answer2',
                                        4 => 'new wrong answer3',
                                    ],
                                    'correctAnswer' => 2,
                                ],
                            ],
                        ],
                        $quiz->getQuiz()->getQuizData()->toArray()
                    );
                    $phpunit->assertContainsOnlyInstancesOf(Quiz\Question::class, $quiz->getQuiz()->getQuestions());
                    $phpunit->assertSame(2, $quiz->getQuiz()->getQuestions()[0]->getCorrectAnswerId());
                    $phpunit->assertSame('new correct answer', $quiz->getQuiz()->getQuestions()[0]->getCorrectAnswer());
                },
            ],
            'update document url' => [
                'identity' => 1,
                'data' => [
                    'document' => 'https://www.youtube.com/new-document-link',
                ],
                'entityName' => Document::class,
                'fixtureName' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Document::class,
                ],
                'expectedChanges' => [
                    'document' => 'https://www.youtube.com/new-document-link',
                ]
            ],
            'update image url' => [
                'identity' => 1,
                'data' => [
                    'image' => 'img/8df7b73a7820f4aef47864f2a6c5fccf',
                ],
                'entityName' => Image::class,
                'fixtureName' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Image::class,
                ],
                'expectedChanges' => [
                    'image' => 'img/8df7b73a7820f4aef47864f2a6c5fccf',
                ]
            ],
        ];
    }

    /**
     * dataProvider For testDeleteSuccess
     *
     * Format: [
     *  'identity' => int,
     *  'fixtures' => array(список классов fixtures),
     *  'asserts' => Closure
     * ]
     *
     * @return array
     */
    public function dataDeleteSuccess()
    {
        return [
            'delete article' => [
                'identity' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Article::class,
                ],
            ],
            'delete video' => [
                'identity' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Video::class,
                ],
            ],
            'delete audio' => [
                'identity' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Audio::class,
                ],
            ],
            'delete quiz' => [
                'identity' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Quiz::class,
                ],
            ],
            'delete document' => [
                'identity' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Document::class,
                ],
            ],
            'delete image' => [
                'identity' => 1,
                'fixtures' => [
                    \FunctionalTest\Application\Service\User\Fixture\UserFixture::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Course::class,
                    \FunctionalTest\Application\Service\Course\Fixture\Chapter::class,
                    \FunctionalTest\Application\Service\Course\Item\Fixture\Image::class,
                ],
            ],
        ];
    }
}
