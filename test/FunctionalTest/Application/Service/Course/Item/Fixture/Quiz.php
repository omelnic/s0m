<?php
namespace FunctionalTest\Application\Service\Course\Item\Fixture;

use Application\Entity\Course\Item\Quiz\QuizVO;
use Application\Entity\Course\Item\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Quiz
 *
 * @author Shahnovsky Alex
 */
class Quiz extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $quiz = new \Application\Entity\Course\Item\Quiz();
        $quiz->setName('Quiz name');
        $quiz->setDescription('Quiz description');
        $quiz->setIsFree(true);
        $quiz->setStartDate(new \DateTime());
        $quiz->setState(State::createFromScalar(State::ACTIVE));
        $quiz->setQuiz(
            QuizVO::createFromArray(
                [
                    'questions' => [
                        [
                            'question' => 'Who?',
                            'answers' => [
                                1 => 'correct answer',
                                2 => 'wrong answer1',
                                3 => 'wrong answer2',
                                4 => 'wrong answer3',
                            ],
                            'correctAnswer' => 1,
                        ],
                    ],
                ]
            )
        );
        $chapter = $this->getReference('course_chapter');
        $quiz->setChapter($chapter);

        $manager->persist($quiz);
        $manager->flush();
    }
}
