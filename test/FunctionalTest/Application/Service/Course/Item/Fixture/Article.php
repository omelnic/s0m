<?php
namespace FunctionalTest\Application\Service\Course\Item\Fixture;

use Application\Entity\Course\Item\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class Article
 *
 * @author Shahnovsky Alex
 */
class Article extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $article = new \Application\Entity\Course\Item\Article();
        $article->setName('Article name');
        $article->setDescription('Article description');
        $article->setIsFree(true);
        $article->setStartDate(new \DateTime());
        $article->setState(State::createFromScalar(State::ACTIVE));
        $article->setContent('Article content');
        $chapter = $this->getReference('course_chapter');
        $article->setChapter($chapter);

        $manager->persist($article);
        $manager->flush();
    }
}
