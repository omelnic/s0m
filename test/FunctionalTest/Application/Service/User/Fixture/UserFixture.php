<?php
namespace FunctionalTest\Application\Service\User\Fixture;

use Application\Entity\User\Admin;
use Application\Entity\User\State;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class UserFixture
 *
 * @author Shahnovsky Alex
 */
class UserFixture extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new Admin();
        $admin->setEmail('test@test.com');
        $admin->setBirthday(new \DateTime('01-01-1998'));
        $admin->setState(new State(State::INACTIVE));

        $manager->persist($admin);
        $manager->flush();
        $this->addReference('user_admin_fixture', $admin);

        $admin = new Admin();
        $admin->setEmail('user2@test.com');
        $admin->setBirthday(new \DateTime('01-01-1999'));
        $admin->setState(new State(State::ACTIVE));

        $manager->persist($admin);
        $manager->flush();
    }
}
