<?php
namespace FunctionalTest\Application\Service\User;

use Application\Entity\User\Admin;
use Application\Entity\User\State;
use Application\Service\User\User;
use FunctionalTest\Application\Service\User\Fixture\UserFixture;
use FunctionalTest\Framework\ServiceTestCase;

/**
 * Class UserTest
 *
 * @author Shahnovsky Alex
 */
class UserTest extends ServiceTestCase
{

    /**
     * Возвращает имя сервиса в ServiceManager
     *
     * @return string
     */
    protected function getServiceName()
    {
        return User::class;
    }

    /**
     * Возвращает тип сервиса (имя класса)
     *
     * @return string
     */
    protected function getServiceType()
    {
        return User::class;
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'data' => [....],
     *      'fixtures' => array()
     *      'entityName' => string,
     *      'callback' => Closure|null
     *      'role' => array|string|null
     * ]
     *
     * @return array
     */
    public function dataCreateSuccess()
    {
        return [
            'validData' => [
                'data' => [
                    'email' => 'test@test.com',
                    'password' => 'password1234',
                    'state' => State::ACTIVE,
                    'type' => 'admin',
                    'birthday' => '01-01-1999',
                ],
                'fixtures' => [
                ],
                'entityName' => Admin::class,
            ],
        ];
    }

    /**
     * dataProvider for testCreateFailed
     *
     * Format: [
     *  'data' => array(invalid data),
     *  'fixtures' => array(),
     *  'messageKeys' => array(список всех ожидаемых ключей)
     * ]
     *
     * @return array
     */
    public function dataCreateFailed()
    {
        return [
            'empty' => [
                'data' => [
                    'type' => 'admin',
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'email',
                    'password',
                    'birthday',
                ]
            ],
        ];
    }

    /**
     * dataProvider For testUpdateSuccess
     *
     * Format: [
     *  'id' => int,
     *  'data' => array(...),
     *  'entityName' => 'имя entity',
     *  'fixtureName' => array(список классов fixtures),
     *  'expectedChanges' => array(список колонок в бд и их значения)
     *  'callback' => function($updateEntity, $this) {}  - если нужны доп проверки
     * ]
     *
     * @return array
     */
    public function dataUpdateSuccess()
    {
        return [
            'updateTextValues' => [
                'identity' => 1,
                'data' => [
                    'password' => 'newpwd',
                    'state' => State::ACTIVE,
                ],
                'entityName' => Admin::class,
                'fixtureName' => [
                    UserFixture::class,
                ],
                'expectedChanges' => [
                    'state_state' => '1',
                ]
            ],
        ];
    }

    /**
     * dataProvider For testDeleteSuccess
     *
     * Format: [
     *  'identity' => int,
     *  'fixtures' => array(список классов fixtures),
     *  'asserts' => Closure
     * ]
     *
     * @return array
     */
    public function dataDeleteSuccess()
    {
        return [
            'delete' => [
                'identity' => 1,
                'fixtures' => [
                    UserFixture::class,
                ]
            ]
        ];
    }
}
