<?php
/**
 * Created by Topuria Evgheni.
 * Date: 6/9/17
 */

namespace FunctionalTest\Application\Service\Subscription;

use Application\Service\Subscription\HistorySubscriptionPackage;
use FunctionalTest\Application\Service\Subscription\Fixture\ActiveSubscriptionPackageFixture;
use FunctionalTest\Application\Service\Subscription\Fixture\SubscriptionPackageFixture;
use FunctionalTest\Application\Service\User\Fixture\UserFixture;
use FunctionalTest\Framework\ServiceTestCase;

class HistorySubscriptionPackageTest extends ServiceTestCase
{

    /**
     * Возвращает имя сервиса в ServiceManager
     *
     * @return string
     */
    protected function getServiceName()
    {
        return HistorySubscriptionPackage::class;
    }

    /**
     * Возвращает тип сервиса (имя класса)
     *
     * @return string
     */
    protected function getServiceType()
    {
        return HistorySubscriptionPackage::class;
    }

    /**
     * dataProvider for testCreateSuccess.
     * Format: [
     *      'data' => [....],
     *      'fixtures' => array()
     *      'entityName' => string,
     *      'callback' => Closure|null
     *      'role' => array|string|null
     * ]
     *
     * @return array
     */
    public function dataCreateSuccess()
    {
        $startDate = new \DateTime('2016-10-01 12:05:12');
        $endDate = new \DateTime('2017-01-01 12:05:12');

        return [
            'validData' => [
                'data' => [
                    'user' => '1',
                    'subscriptionPackage' => '1',
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                ],
                'fixtures' => [
                    UserFixture::class,
                    SubscriptionPackageFixture::class,
                ],
                'entityName' => \Application\Entity\Subscription\ActiveSubscriptionPackage::class,
            ],
        ];
    }

    /**
     * dataProvider for testCreateFailed
     *
     * Format: [
     *  'data' => array(invalid data),
     *  'fixtures' => array(),
     *  'messageKeys' => array(список всех ожидаемых ключей)
     * ]
     *
     * @return array
     */
    public function dataCreateFailed()
    {
        return [
            'empty' => [
                'data' => [
                ],
                'fixtures' => [],
                'messageKeys' => [
                    'user',
                    'subscriptionPackage',
                    'startDate',
                    'endDate',
                ]
            ],
        ];
    }

    /**
     * dataProvider For testUpdateSuccess
     *
     * Format: [
     *  'id' => int,
     *  'data' => array(...),
     *  'entityName' => 'имя entity',
     *  'fixtureName' => array(список классов fixtures),
     *  'expectedChanges' => array(список колонок в бд и их значения)
     *  'callback' => function($updateEntity, $this) {}  - если нужны доп проверки
     * ]
     *
     * @return array
     */
    public function dataUpdateSuccess()
    {
        $startDate = new \DateTime('2015-10-01 10:05:59');
        $endDate = new \DateTime('2016-01-01 11:05:55');

        return [
            'updateTextValues' => [
                'identity' => 1,
                'data' => [
                    'user' => '2',
                    'subscriptionPackage' => '2',
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                ],
                'entityName' => \Application\Entity\Subscription\ActiveSubscriptionPackage::class,
                'fixtureName' => [
                    UserFixture::class,
                    SubscriptionPackageFixture::class,
                    ActiveSubscriptionPackageFixture::class,
                ],
                'expectedChanges' => [
                    'user_id' => '2',
                    'subscription_package_id' => '2',
                    'start_date' => '2015-10-01 10:05:59',
                    'end_date' => '2016-01-01 11:05:55',
                ]
            ],
        ];
    }

    /**
     * dataProvider For testDeleteSuccess
     *
     * Format: [
     *  'identity' => int,
     *  'fixtures' => array(список классов fixtures),
     *  'asserts' => Closure
     * ]
     *
     * @return array
     */
    public function dataDeleteSuccess()
    {
        return [
            'delete' => [
                'identity' => 1,
                'fixtures' => [
                    UserFixture::class,
                    SubscriptionPackageFixture::class,
                    ActiveSubscriptionPackageFixture::class,
                ]
            ]
        ];
    }
}
