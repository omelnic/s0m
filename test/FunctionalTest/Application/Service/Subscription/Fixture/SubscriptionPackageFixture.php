<?php
namespace FunctionalTest\Application\Service\Subscription\Fixture;

use Application\Entity\Subscription\SubscriptionPackage;
use Application\Entity\ValueObject\Period;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class SubscriptionPackageFixture
 *
 * @author Shahnovsky Alex
 */
class SubscriptionPackageFixture extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $subsPackage = new SubscriptionPackage();
        $subsPackage->setName('S0m somich');
        $subsPackage->setPrice('19.99');
        $subsPackage->setPeriod(new Period(Period::PERIOD_THREE_MONTH));

        $manager->persist($subsPackage);
        $manager->flush();
        $this->addReference('subscription_package', $subsPackage);

        $subsPackage = new SubscriptionPackage();
        $subsPackage->setName('S0m somich trial');
        $subsPackage->setPrice('0.99');
        $subsPackage->setPeriod(new Period(Period::PERIOD_ONE_YEAR));

        $manager->persist($subsPackage);
        $manager->flush();
    }
}
