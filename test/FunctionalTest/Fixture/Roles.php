<?php
namespace FunctionalTest\Fixture;

use CirclicalUser\Entity\Authentication;
use Doctrine\Common\DataFixtures\AbstractFixture;

/**
 * Class Roles
 *
 * @author Shahnovsky Alex
 */
class Roles extends AbstractFixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $userRole = new \CirclicalUser\Entity\Role();
        $userRole->setName('user');

        $manager->persist($userRole);
        $manager->flush();

        $role = new \CirclicalUser\Entity\Role();
        $role->setName('admin');
        $role->setParent($userRole);

        $manager->persist($role);
        $manager->flush();

        $role = new \CirclicalUser\Entity\Role();
        $role->setName('student');
        $role->setParent($userRole);

        $manager->persist($role);
        $manager->flush();

        $role = new \CirclicalUser\Entity\Role();
        $role->setName('professor');
        $role->setParent($userRole);

        $manager->persist($role);
        $manager->flush();
    }
}
