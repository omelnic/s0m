<?php
namespace FunctionalTest\Acl;

use CirclicalUser\Mapper\RoleMapper;
use FunctionalTest\TestCase as MainTestCase;

/**
 * Class TestCase
 *
 * @author Shahnovsky Alex
 */
class TestCase extends MainTestCase
{
    /**
     * @return array
     */
    protected function getRoles()
    {
        return [
            'user',
            'professor',
            'student',
            'admin',
            'group-less',
        ];
    }

    public function testAllRolesPresent()
    {
        /** @var RoleMapper $service */
        $service = $this->getService(RoleMapper::class);
        $this->setAuthenticationRole('admin');
        $actualRoles = $service->getAllRoles();

        $this->assertCount(count($this->getRoles()) - 1, $actualRoles);

        foreach ($actualRoles as $role) {
            if ($role == 'group-less') {
                continue;
            }
            $this->assertContains($role->getName(), $this->getRoles());
        }
    }

    /**
     * @param $expected
     * @param $role
     *
     * @return string
     */
    protected function getExpectedResult($expected, $role)
    {
        if (isset($expected[$role])) {
            $expectedForRole = $expected[$role];
        } elseif (isset($expected['*'])) {
            $expectedForRole = $expected['*'];
        } else {
            $this->fail(sprintf('Не указан случай для роли: %s', $role));
            return null;
        }

        return $expectedForRole;
    }

    protected function getMessage($bool)
    {
        return $bool?'Разрешен':'Запрещен';
    }
}
