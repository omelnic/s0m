<?php
namespace FunctionalTest;

/**
 * Class Module
 *
 * @author Shahnovsky Alex
 */
class Module
{
    public function getConfig()
    {
        return include __DIR__.'/../config/module.config.php';
    }
}
