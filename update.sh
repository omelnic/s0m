#!/bin/sh

CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
echo "------- run: git pull origin $CURRENT_BRANCH \n"
git pull origin $CURRENT_BRANCH

echo '------- run: composer install'
composer install

echo '------ run: ./vendor/bin/doctrine-module migrations:migrate --no-interaction'
php ./vendor/bin/doctrine-module migrations:migrate --no-interaction

echo '------- run: php ./vendor/bin/doctrine-module orm:generate:proxies'
php ./vendor/bin/doctrine-module orm:generate:proxies
