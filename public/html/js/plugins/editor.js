/**
 * Created by admin on 09.06.2017.
 */
$(document).ready(function(){
	textEditor();

	function textEditor() {
		var toolbarOptions = [
			['bold', 'italic', 'underline', 'strike'],
			[{ 'list': 'ordered'}, { 'list': 'bullet' }]
		];

		var quill = new Quill('.content-editor', {
			modules: {
				toolbar: toolbarOptions
			},
			theme: 'snow'
		});
	}
});
