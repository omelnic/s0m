$( document ).ready(function() {
    console.log('custom')
    var el = document.getElementById('chapters');
    if(el){
        var sortable = Sortable.create(el);
    }
    var editor = new Simditor({
        textarea: $('#editor'),
        toolbar: [
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'ol',
            'ul',
            'blockquote',
            'code'
        ]
    });
    selectTab();
});

function selectTab(){
    $('.select-tab').on('change', function(e){
        e.stopPropagation();
        var self = $(this);
        var value = self.val();
        $('.select-tab-content').css('display', 'none');
        $('#'+value).css('display', 'block');
    });
}