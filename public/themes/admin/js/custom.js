$( document ).ready(function() {
    console.log('custom')
    var el = document.getElementById('chapters');
    if(el){
        var sortable = Sortable.create(el);
    }
    tinymce.init({ selector:'.editor' });

    selectTab();
    addQuestion();
    wysiwygTab();
});

function wysiwygTab() {

};

function selectTab() {
    $('.select-type').on('change', function(e){
        e.stopPropagation();
        var self = $(this);
        var value = self.val();
        console.log('valuedd', value);
        $('.select-tab-content').css('display', 'none');
        $('#'+value).css('display', 'block');
    });
}

function addQuestion() {
    var question = '.question-item';
    $('.add-question').click(function (e) {
        e.preventDefault();
        var pos = $('.question-item').length - 1;
        var questions = $('.templates ' + question).clone();

        questions.find(".quizQuestion").attr("name", "quiz[questions]["+ pos +"][question]");
        questions.find(".quizAnswers").attr("name", "quiz[questions]["+ pos +"][answers][]");
        questions.find(".correctAnswer").attr("name", "quiz[questions]["+ pos +"][correctAnswer]");
        questions.find(".correctAnswer").each(function() {
            $newId = $(this).attr("id").replace(/^val\-\d+\-/, "val-" + pos + "-");
            $(this).closest(".pb-1em").find("label").attr("for", $newId);
            $(this).attr("id", $newId);
        });

        questions.appendTo('#sortItems');
    });
    $('body').on('click', '.add-option', function (e) {
        e.preventDefault();
        var pos = $(this).closest('.question-item').find('.items-center').length;
        var lastQuestion = $(this).closest('.question-item').find('.items-center').last().clone();
        $newId = lastQuestion.find(".correctAnswer").attr("id").replace(/\-\d+$/, "-" + pos);

        lastQuestion.find("label").attr("for", $newId);
        lastQuestion.find(".correctAnswer").attr("id", $newId).attr("value", pos);
        lastQuestion.find(".quizAnswers").val("");

        lastQuestion.appendTo($(this).closest(".question-item").find('.options-list'));
    });
    $('body').on('click', '.remove-option', function (e) {
        e.preventDefault();
        $(this).closest('.question-item').find('.items-center').last().remove();
    });
    $('body').on('click', '.remove-question', function (e) {
        e.preventDefault();
        $(this).closest(question).remove();
    });
}